module animation

import math
import mathhelper
import curves

pub struct Animation {
pub mut:
	progress f32
	running bool
	from f32
	target ?&f32
	to f32 = 1
	duration f32 = 1
	curve fn (f32) f32 = curves.curve['linear']
	repeat int
	onfinish ?fn()
}

pub fn (mut a Animation) start() {
	if target := a.target {
		a.from = *target
	}
	a.running = true
}

pub fn (mut a Animation) move(value f32) {
	if !a.running { return }

	if a.duration == 0 {
		a.progress = 1
	} else {
		a.progress += value / a.duration
		a.progress = f32(math.clamp(a.progress, 0, 1))
	}

	if target := a.target {
		target_value := mathhelper.lerp(a.from, a.to, a.curve(a.progress))
		min := math.min(a.from, a.to)
		max := math.max(a.from, a.to)
		(*target) = f32(math.clamp(target_value, min, max))// linearly interpolate target based on progress
	}

	if a.is_complete() {
		a.running = false
		if onfinish := a.onfinish {
			onfinish()
		}
	}
}

pub fn (mut a Animation) skip() {
	if target := a.target {
		(*target) = a.to * a.curve(1)
	}

	a.progress = 1
	a.running = false
}

pub fn (a Animation) is_complete() bool {
	return a.progress >= 1
}
