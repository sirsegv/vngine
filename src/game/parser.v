module game

pub fn (mut s Scene) process_line() {
	mut done := false
	
	for !done {
		line := s.script[s.current_line]
		
		if line.starts_with('[') {
			done = s.process_action(line)
		} else {
			done = true
		}
	}
}

pub fn (mut s Scene) process_action(line string) bool {
	command := line.trim('[]').split(' ')
	if command[0] == 'char' {
		s.add_character(command)
	} else if command[0] == 'move' {
		s.move_character(command)
	} else if command[0] == 'background' {
		s.set_background(command)
	} else if command[0] == 'delay' {
		s.delay(command)
		return true
	} else if command[0] == 'rmchar' {
		s.remove_character(command)
	} else if command[0] == 'sprite' {
		s.set_character_sprite(command)
	} else {
		eprintln('Unknown command ${command[0]}')
	}

	s.current_line++
	return false
}