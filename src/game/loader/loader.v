module loader

import os
import toml

const (
	game_file_extension = '.vn'
)

pub fn load_game[T](path string) T {
	game_files := os.ls(path) or {
		eprintln('Cannot find directory ${path}')
		exit(1)
	}

	for file in game_files {
		if os.file_ext(file) == game_file_extension {
			data := toml.parse_file(os.join_path(path, file)) or {
				eprintln('Cannot parse game file')
				exit(1)
			}
			result := data.reflect[T]()
			return result
		}
	}

	eprintln('Cannot find game file')
	exit(1)
}