module game

import irishgreencitrus.raylibv as r
import os
import animation
import animation.curves
import character

pub fn (mut s Scene) add_character(info []string) {
	fade := info[2] or {'0'}.f32()

	mut new_char := character.load(os.join_path(s.game_path, s.meta.characters_folder), info, fade) or {
		eprintln(err.msg())
		return
	}

	s.characters[info[1]] = &new_char
}

pub fn (mut s Scene) remove_character(info []string) {
	if info[1] in s.characters {
		s.characters.delete(info[1])
	} else {
		eprintln('Cannot remove character ${info[1]} which is not in scene')
	}
}

pub fn (mut s Scene) move_character(info []string) {
	mut target := &f32(0)

	c := s.characters[info[1]] or {
		eprintln('Cannot move nonexistant character ${info[1]}')
		return
	}
	if info[2] == 'x' {
		target = &(c.pos.x)
	}
	else if info[2] == 'y' {
		target = &(c.pos.y)
	}

	if target == &f32(0) {
		eprintln('Failed to move character ${info[1]}')
		return
	}

	mut move_anim := animation.Animation { target: target }

	move_anim.curve = curves.curve[info[5] or {'outexpo'}]
	move_anim.duration = info[4].f32()
	move_anim.to = info[3].f32()

	move_anim.start()

	s.animations << move_anim
}

pub fn (mut s Scene) set_background(info []string) {
	background_files := os.ls(os.dir(os.join_path(s.game_path, s.meta.backgrounds_folder, info[1]))) or {
		eprintln('Could not find backgrounds folder')
		return
	}

	for file in background_files {
		if file.starts_with(os.file_name(info[1])) {
			s.background = r.load_texture(os.join_path(s.game_path, s.meta.backgrounds_folder, file).str)
			return
		}
	}

	eprintln('Could not find background ${info[1]}')
}

pub fn (mut s Scene) delay(info []string) {
	onfinish := fn [mut s] () {
		s.next_line() or { return }
	}

	mut delay := animation.Animation { duration: info[1].f32(), onfinish: onfinish }
	delay.start()
	s.line_animations << delay
}

pub fn (mut s Scene) set_character_sprite(info []string) {
	selecter_char := s.characters[info[1]] or {
		eprintln('Character ${info[1]} not found in scene')
		return
	}
	texture := selecter_char.load_sprite(info[2]) or { return }

	duration := info[3] or {'0'}.f32()

	if anim := s.characters[info[1]].sprite.remove_texture(false, duration) {
		s.animations << anim
	}
	if anim := s.characters[info[1]].sprite.add_texture(texture, duration) {
		s.animations << anim
	}
}
