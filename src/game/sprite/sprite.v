module sprite

import irishgreencitrus.raylibv as r
import animation
import screen

pub struct Sprite {
pub mut:
	scale f32
	textures_opacity []f32
	textures []r.Texture2D
}

pub fn (mut s Sprite) add_texture(tex r.Texture2D, duration f32) ?animation.Animation {
	s.textures << tex

	if duration == 0 {
		s.textures_opacity << 1
		return none
	} else {
		s.textures_opacity << 0
		fade := &s.textures_opacity[s.textures_opacity.len - 1]

		mut fadeanim := animation.Animation { target: fade, duration: duration, to: 1 }
		fadeanim.start()
		return fadeanim
	}
}

pub fn (mut s Sprite) remove_texture(fade bool, duration f32) ?animation.Animation {
	if duration == 0 {
		s.textures.pop()
		s.textures_opacity.pop()
		return none
	} else {
		texture_index := s.textures_opacity.len - 1

		onfinish := fn [mut s, texture_index] () {
			s.textures.delete(texture_index)
			s.textures_opacity.delete(texture_index)
		}

		mut anim := animation.Animation { duration: duration, to: 0, onfinish: onfinish }
		if fade {
			opacity := &s.textures_opacity[s.textures_opacity.len - 1]
			anim.target = opacity
		}
		anim.start()

		return anim
	}
}

pub fn (s Sprite) draw_centered(pos r.Vector2, rotation f32, scale f32) {
	for i, t in s.textures {
		draw_pos := screen.pos(
			int(pos.x - ((t.width * scale) / 2)),
			int(pos.y - ((t.height * scale) / 2)),
		)

		draw_scale := f32(screen.scale(1)) * scale

		r.draw_texture_ex(
			t,
			draw_pos,
			rotation,
			draw_scale,
			r.Color { 255, 255, 255, u8(255 * s.textures_opacity[i]) }
		)
	}
}

pub fn (s Sprite) draw_absolute(pos r.Vector2, rotation f32, scale f32) {
	for i, t in s.textures {
		r.draw_texture_ex(
			t,
			pos,
			rotation,
			scale,
			r.Color { 255, 255, 255, u8(255 * s.textures_opacity[i]) }
		)
	}
}