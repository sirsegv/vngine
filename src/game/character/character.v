module character

import os
import toml
import irishgreencitrus.raylibv as r
import sprite

pub const (
	char_file_extension = '.chr'
	texture_formats = ['.png', '.jpg', '.jpeg', '.bmp']
)

// [heap]
pub struct Character {
mut:
	path string
pub mut:
	name string
	color r.Color
	scale f32
	rotation f32
	pos r.Vector2
	sprite sprite.Sprite
}

pub fn (mut c Character) set_sprite(texture string, duration f32) {
	loaded_sprite := c.load_sprite(texture) or { return }
	c.sprite.add_texture(loaded_sprite, duration)
}

pub fn (c Character) load_sprite(texture string) ?r.Texture2D {
	sprite_path := c.get_sprite_path(texture) or {
		eprintln('${c.name} does not have sprite ${texture}')
		return none
	}

	return r.load_texture(sprite_path.str)
}

pub fn (c Character) get_sprite_path(texture string) ?string {
	sprite_files := os.ls(c.path) or {
		eprintln('Cannot find character sprites folder ${c.path}')
		return none
	}

	for file in sprite_files {
		if file.starts_with(texture) && os.file_ext(file) in texture_formats {
			return os.join_path(c.path, file)
		}
	}

	return none
}

// Load character from file
pub fn load(path string, info []string, fade f32) !Character {
	// create new character
	mut new_char := character.Character {}

	// locate and load character file
	mut char_found := false
	mut char_data := toml.Doc {}

	char_list := os.walk_ext(path, char_file_extension)

	if char_list.len == 0 { error('No character files found') }

	for file in char_list {
		if os.file_name(file).to_lower().starts_with(info[1].to_lower()) {
			new_char.path = os.dir(file)
			char_data = toml.parse_file(file) or {
				return error('Cannot parse character file')
			}
			char_found = true
			break
		}
	}

	if !char_found { error('Cannot find character file for ${info[1]}') }

	new_char.name = char_data.value('name').string()
	new_char.color = r.Color { u8(char_data.value('color[0]').int()), u8(char_data.value('color[1]').int()), u8(char_data.value('color[2]').int()), 255 }
	new_char.pos = r.Vector2 { char_data.value('position[0]').int(), char_data.value('position[1]').int() }
	new_char.scale = char_data.value('scale').f32()
	new_char.set_sprite(char_data.value('sprite').string(), fade)

	return new_char
}
